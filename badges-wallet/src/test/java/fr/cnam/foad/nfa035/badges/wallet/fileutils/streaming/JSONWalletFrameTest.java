package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming;

import static org.junit.jupiter.api.Assertions.*;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JSONWalletFrameTest {

    private static final Logger LOG = LogManager.getLogger(JSONWalletFrameTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.json");
    private RandomAccessFile random;

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    /**
     * test de l'encodage de l'image sur le flux d'écriture
     * @throws IOException
     */
    @Test
    public void testGetEncodedImageOutput() throws IOException {

    }


    /**
     * test de l'encodage de l'image si database vide
     * @throws IOException
     */
    @Test
    void testGetEncodedImageOutputWhenDatabaseIsEmpty() throws IOException {

        random = new RandomAccessFile(walletDatabase, "r");
        JSONWalletFrame media = new JSONWalletFrame(random);
        String  encode = media.getEncodedImageOutput().toString();

        LOG.info("image encodée" + encode);
        assertNotNull(encode);
    }
}
