package fr.cnam.foad.nfa035.badges.wallet.dao.json;


import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAOTest;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class JSONBadgeWalletDAOTest {
    private static final Logger LOG = LogManager.getLogger(BadgeWalletDAOTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File("db_wallet.json");

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }
    /**
     * Teste l'ajout d'un badge
     */
    @Test
    public void testAddBadgeOnDatabaseException() {
try {

    JSONBadgeWalletDAOImpl daoJson = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    Date begin = format.parse("2021-11-23");
    Date end = format.parse("2023-01-30");

    File image = new File(RESOURCES_PATH + "superman.jpg");
    DigitalBadge badge = new DigitalBadge ("NFA040", begin, end, null, image);
    daoJson.addBadge(badge);

    assertThrows(IOException.class, ()-> {
        daoJson.addBadge(badge);
    });


    }catch (Exception e) {
     LOG.error("Test en échec", e);
        fail();
}
    }

    /**
     * Teste l'ajout d'un badge dans la database sur Base multi-badge
     */
    @Test
    public void testAddBadgeOnDatabase(){
        try {
        JSONBadgeWalletDAOImpl daoJson = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");

        Set<DigitalBadge> metas = null;

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date begin = format.parse("2021-11-23");
        Date end = format.parse("2023-01-30");

        //1er Badge
        File image = new File(RESOURCES_PATH + "superman.jpg");
        DigitalBadge badge = new DigitalBadge ("NFA040", begin, end, null, image);
        daoJson.addBadge(badge);

        //2eme badge
        File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
        DigitalBadge badge2 = new DigitalBadge("NFA041", begin, end, null, image2);
        daoJson.addBadge(badge2);

            try (BufferedReader reader = new BufferedReader(new FileReader(walletDatabase))) {

                JSONWalletSerializerDAImpl serializer = new JSONWalletSerializerDAImpl(metas);
                JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"));

               String serializedImage = reader.readLine();
               LOG.info("1er badge: \n []", serializedImage);
               serializer.serialize(badge, media);
               String data = serializedImage.split(",\\{\"payload\":")[1].split("\"\\}]")[0];

                String serializedImage2 = reader.readLine();
                LOG.info("2eme badge: \n []", serializedImage);
                serializer.serialize(badge, media);
                String data2 = serializedImage.split(",\\{\"payload\":")[1].split("\"\\}]")[0];

               assertEquals(1, Integer.parseInt(data));

               assertEquals(2, Integer.parseInt(data2));

            }

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }


    /**
     * Teste la récupération des Métadonnées en mode String
     */
    @Test
    void testGetMetadata() throws IOException {

        JSONBadgeWalletDAOImpl daoJson = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");
        Set<DigitalBadge> metas = daoJson.getWalletMetadata();
        LOG.info("Et voici les métadonnées : []", daoJson.getWalletMetadata());

        assertEquals(2, metas.size());
        Iterator<DigitalBadge> it = new TreeSet(metas).iterator();
        assertEquals(new DigitalBadgeMetadata(1, 0, 557), it.next().getMetadata());
        assertEquals(new DigitalBadgeMetadata(2, 782, 906), it.next().getMetadata());

    }

    /**
     * Test la récupération séquentielle des badges du wallet
     */
    @Test
    public void testGetBadgeFromDatabaseByMetadata(){

        try {
            BadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");
            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
            File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
            File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
            File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");
            DigitalBadge expectedBadge1 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(1, 0,557), null);
            DigitalBadge expectedBadge2 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(2, 782,906), null);
            DigitalBadge expectedBadge3 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(3, 2030,35664), null);

            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            ((JSONBadgeWalletDAOImpl)dao).getBadgeFromMetadata(fileBadgeStream1, expectedBadge1);
            OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
            ((JSONBadgeWalletDAOImpl)dao).getBadgeFromMetadata(fileBadgeStream2, expectedBadge2);
            OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
            ((JSONBadgeWalletDAOImpl)dao).getBadgeFromMetadata(fileBadgeStream3, expectedBadge3);

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
            LOG.info("Badge 2 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
            LOG.info("Badge 3 récupéré avec succès");

        }catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }

    }


    }


