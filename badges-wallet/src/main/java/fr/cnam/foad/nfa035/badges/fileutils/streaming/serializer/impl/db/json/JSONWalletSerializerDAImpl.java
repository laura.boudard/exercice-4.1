package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.proxy.Base64OutputStreamProxy;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.commons.codec.binary.Base64OutputStream;
import java.io.*;
import java.util.Set;

public class JSONWalletSerializerDAImpl extends AbstractStreamingImageSerializer<DigitalBadge, WalletFrameMedia> {

    Set<DigitalBadge> metas;

    public JSONWalletSerializerDAImpl(Set<DigitalBadge> metas) {
        this.metas = metas;
    }

    /**
     * Utile pour récupérer un Flux de lecture de la source à sérialiser
     *
     * @param source
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getSourceInputStream(DigitalBadge source) throws IOException {
        return new FileInputStream(source.getBadge());
    }

    /**
     * Permet de récupérer le flux d'écriture et de sérialisation vers le media
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(WalletFrameMedia media) throws IOException {
        return new Base64OutputStreamProxy(new Base64OutputStream(media.getEncodedImageOutput(), true, 0, null));
    }

    /**
     * Sérialise une image au format json depuis un support quelconque vers un media quelconque
     * ObjectMapper : fournit des fonctionnalités pour lire et écrire avec données format json
     *
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public void serialize(DigitalBadge source, WalletFrameMedia media) throws IOException {

        RandomAccessFile random = media.getChannel();
        if (metas.contains(source)){
            throw new IOException("Badge déjà présent dans le Wallet");
        }
        long pos = source.getMetadata().getWalletPosition();
        media.getChannel().seek(pos);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        StringWriter stringWriter = new StringWriter();
        objectMapper.writeValue(stringWriter, getSourceInputStream(source));

        media.incrementLines();
    }
}
