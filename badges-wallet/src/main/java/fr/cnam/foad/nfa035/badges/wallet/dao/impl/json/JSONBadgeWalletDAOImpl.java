package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;


import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Set;

public class JSONBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(JSONBadgeWalletDAOImpl.class);

    private final File walletDatabase;

    /**
     * Constructeur
     *
     * @param dbPath
     * @throws IOException
     */
    public JSONBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     * @hidden
     * @deprecated Sur les nouveaux formats de base CVS ou JSON, Utiliser de préférence addBadge(DigitalBadge badge)
     */
    @Override
    public void addBadge(File image) throws IOException {
        LOG.error("Non supporté");
        throw new IOException("Méthode non supportée, utilisez plutôt addBadge(DigitalBadge badge)");
    }

    /**
     * Permet d'ajouter le badge au nouveau format de Wallet
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException {
        try (JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            ImageStreamingSerializer serializer = new JSONWalletSerializerDAImpl(getWalletMetadata());
            serializer.serialize(badge, media);
        }
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     * @deprecated Sur les nouveaux formats de base CVS ou JSON, Utiliser de préférence getBadgeFromMetadata
     */
    @Override
    public void getBadge(OutputStream imageStream) throws IOException{
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }


    /**
     * {@inheritDoc}
     *
     * @return List<DigitalBadgeMetadata>
     */
    @Override
    public Set<DigitalBadge> getWalletMetadata() throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            return new MetadataDeserializerDatabaseImpl().deserialize(media);
        }
    }

    /**
     *Permet d'accéder au métadata du portefeuille de badge
     *
     * @return List<DigitalBadgeMetadata>
     * @throws IOException
     */
    private Set<DigitalBadge> getWalletMetadata (String jsonMode) throws IOException {
        try(JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "r")))
        {
            return new MetadataDeserializerJSONImpl().deserialize(media);
        }
    }
        /**
         * {@inheritDoc}
         *
         * @param imageStream
         * @param meta
         * @throws IOException
         */
        @Override
        public void getBadgeFromMetadata (OutputStream imageStream, DigitalBadge meta) throws IOException {
            Set<DigitalBadge> metas = this.getWalletMetadata();
            try (JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
                new JSONWalletDeserializerDAImpl(imageStream, metas).deserialize(media, meta);
            }
        }
    }


