package fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;


public class JSONWalletFrame extends WalletFrame {

    private static final Logger LOG = LogManager.getLogger(JSONWalletFrame.class);
    private FileOutputStream encodedImageOutput = null;

    /**
     * Constructeur élémentaire
     *
     * @param walletDatabase
     */
    public JSONWalletFrame(RandomAccessFile walletDatabase) {
        super(walletDatabase);
    }

    /**
     * {@inheritDoc}
     * <p>
     * Permet l'initialisation du fichier json (amorce) si celui-ci est vierge,
     * sinon récupère l'amorce et l'utilise pour initialiser les méta-données
     * globales du média (nombre de lignes...)
     *
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {

        if (encodedImageOutput == null) {

            RandomAccessFile file = getChannel();
            encodedImageOutput = new FileOutputStream(file.getFD());
            long fileLength = file.length();

            JsonGenerator generator = new JsonFactory().createGenerator(encodedImageOutput, JsonEncoding.UTF8);
            ObjectMapper objectMapper = new ObjectMapper();

            if (fileLength == 0) {
                generator.writeStartObject();
                generator.writeArrayFieldStart("[");
                }
            else {
                BufferedReader br = getEncodedImageReader(false);
                String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(file);
                LOG.debug("Dernière ligne du fichier : ]", lastLine);
                String[] data = lastLine.split(",\\{\"payload");
                setNumberOfLines(Long.parseLong(data[0])-2);
                file.seek(fileLength);

            }
            }

        return encodedImageOutput;
    }
}
